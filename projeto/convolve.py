"""
Módulo para convolução de imagens em escala de cinza com uma máscara.

Ao realizar a leitura de imagens, elas passam a ser tratadas como
um array 2D de floats. Assim, não há preocupação com overflow ou valores
negativos. Para salvar, as imagens são mapeadas para o intervalo (0, 255)
ou é realizado um cap para esse intervalo, se desejado.
"""
import numpy as np
import cv2
import os, sys, argparse
from pathlib import PurePath
from scipy.ndimage import convolve
from mascaras import mascaras


def le_imagem_cinza(arquivo: str):
    """
    Retorna uma imagem em escala de cinza (array 2D) com
    valores do tipo float referente ao arquivo especificado.
    """
    return cv2.imread(arquivo, flags=cv2.IMREAD_GRAYSCALE).astype(float)


def mapeia(matriz, intervalo: tuple):
    """
    Mapeia a matriz do intevalo (min(imagem), max(imagem))
    para o intervalo especificado na forma (min, max).
    """
    minimo = np.min(matriz)
    maximo = np.max(matriz)

    return (matriz - minimo) * (intervalo[1] - intervalo[0]) / (maximo - minimo) + intervalo[0]


def cap(matriz, intervalo: tuple):
    """
    Faz um cap da matriz para o intervalo no formato (a, b).
    Valores menores que a são convertidos para a e maiores que
    b são convertidos para b.
    """
    saida = matriz.copy()

    saida[saida < a] = a
    saida[saida > b] = b

    return saida


def convolução(imagem, filtro):
    """
    Faz a convolução da imagem com o filtro dado.
    """
    return convolve(imagem, filtro, mode='mirror')


def combina(imagens: list):
    """
    Retorna uma imagem em que cada píxel é a raiz quadrada
    da soma dos quadrados dos píxels correspondentes das
    imagens na lista imagens.
    """
    return np.sqrt(sum([imagem**2 for imagem in imagens]))


def salva_imagem_cinza(local: str, imagem, cap: bool):
    """
    Mapeia ou faz um cap da imagem (dependendo de 'cap')
    para o intervalo (0, 255), converte para o tipo np.uint8
    e salva no local especificado.
    """
    if not cap:
        saida = mapeia(imagem, (0, 255)).astype(np.uint8)
    else:
        saida = cap(imagem, (0, 255)).astype(np.uint8)

    cv2.imwrite(local, saida)


def main():
    """
    Lê os argumentos utilizados e realiza as operações.
    Salva a imagem de entrada no diretório especificado
    com o mesmo nome acrescido de '_[filtro]'.
    """
    # Criação do ArgumentParser para lidar com argumentos da chamada e definição dos argumentos.
    parser = argparse.ArgumentParser(description='Convolui imagens com máscaras.')

    parser.add_argument('IMAGEM', action='store', nargs=1, type=str,
        help='imagem que será processada'
    )

    parser.add_argument('-f', '--filtros', action='store', nargs='+', default=None, type=str,
        help='filtros que serão aplicados'
    )

    parser.add_argument('-o', '--output', action='store', nargs=1, default=[''], type=str,
        help='diretório em que os arquivos de saída serão salvos'
    )

    parser.add_argument('-c', '--combine', action='store_true', default=False,
        help='combina os filtros especificados por meio da raiz da soma quadrática dos resultados'
    )

    parser.add_argument('--cap', action='store_true', default=False,
        help='utiliza um cap nos resultados das operações no lugar de um mapeamento para (0, 255)'
    )


    # Lê os argumentos utilizados.
    args = parser.parse_intermixed_args()


    # Lê a imagem e checa por existência do arquivo.
    entrada = le_imagem_cinza(args.IMAGEM[0])
    if entrada is None:
        raise FileNotFoundError


    # Salva o nome do arquivo.
    nome_entrada = PurePath(args.IMAGEM[0]).name


    # Checa se o diretório especificado termina com '/'.
    if len(args.output[0]) > 0 and args.output[0][-1] != '/':
        diretorio_saida = args.output[0] + '/'
    else:
        diretorio_saida = args.output[0]


    # Checa se devem ser aplicados todos os filtros.
    if args.filtros is None:
        filtros = list()

        # Itera sobre todos os filtros disponíveis.
        for filtro in mascaras:
            filtros.append(filtro)                        

    # Caso em que foram especificados os filtros:
    else:
        filtros = [filtro.upper() for filtro in args.filtros]


    # Caso em que cada filtro é aplicado separadamente.
    if not args.combine:

        # Itera sobre todos os filtros escolhidos.
        for filtro in filtros:
            
            # Define o nome do arquivo de saída.
            nome_output = diretorio_saida + nome_entrada[:-4] + '_' + filtro + '.png'

            # Faz a convolução de cada filtro e salva no devido diretório.
            saida = convolução(entrada, mascaras[filtro])
            salva_imagem_cinza(nome_output, saida, cap=args.cap)

    # Caso em que os filtros são combinados.
    else:
        resultados = list()
        nome_output = diretorio_saida + nome_entrada[:-4]

        # Itera sobre todos os filtros escolhidos.
        for filtro in filtro:

            # Adiciona o filtro usado ao nome do arquivo.
            nome_output += '_' + filtro

            # Faz a convolução e guarda em resultados.
            resultados.append(convolução(entrada, mascaras[filtros]))

        # Finaliza o nome do arquivo.
        nome_output += '.png'

        # Combina os resultados e salva.
        saida = combina(resultados)
        salva_imagem_cinza(nome_output, saida, cap=args.cap)


if __name__ == '__main__':
    main()
