# Convolução de imagens.

O programa faz a convolução de imagens em escala de cinza com máscaras.

Para ajuda com os argumentos possíveis, utilize

> python convolve.py --help

-----------------------

As imagens são lidas utilizando o _openCV_ e as entradas são convertidas para _floats_ para evitar overflow e problemas com negativos. A convolução é feita através do _SciPy_.
